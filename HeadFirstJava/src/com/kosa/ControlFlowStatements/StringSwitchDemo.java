package com.kosa.ControlFlowStatements;

public class StringSwitchDemo {

    public static int getMonthNumber(String month) {

        int monthNumber = 0;

        if (month == null) {
            return monthNumber;
        }

        switch (month.toLowerCase()) {
            case "january":
                monthNumber = 1;
                break;
            case "february":
                monthNumber = 2;
                break;
            case "march":
                monthNumber = 3;
                break;
            case "april":
                monthNumber = 4;
                break;
            case "may":
                monthNumber = 5;
                break;
            case "june":
                monthNumber = 6;
                break;
            case "july":
                monthNumber = 7;
                break;
            case "august":
                monthNumber = 8;
                break;
            case "september":
                monthNumber = 9;
                break;
            case "october":
                monthNumber = 10;
                break;
            case "november":
                monthNumber = 11;
                break;
            case "december":
                monthNumber = 12;
                break;
            default: 
                monthNumber = 0;
                break;
        }

        return monthNumber;
    }

    public static void main(String[] args) {

        String month = "August";
        // 중요! 아래와같이 StringSwitchDemo Class 내에서 new 없이 자기자신의 메소드를 호출할수있는 것은 static으로 해당 메소드가 정의되었기 때문이다.
        // 이 예제는 이러한 static의 특징을 잘보여주는 예시이나,
        // 실무적으로는 Main 역할의 클래스는 별도 파일로 독립되어있어서 이러한 자기참조의 형태는 흔하지 않은것으로 보인다.
        // 한편, 해당 메소드가 private 이상의 public인데, private 조차 class내에서 서로 공유가 가능하기 때문에
        // StringSwitchDemo.getMonthNumber(month) 대신 getMonthNumber(month)도 가능
        int returnedMonthNumber =
            StringSwitchDemo.getMonthNumber(month);

        if (returnedMonthNumber == 0) {
            System.out.println("Invalid month");
        } else {
            System.out.println(returnedMonthNumber);
        }
    }
}