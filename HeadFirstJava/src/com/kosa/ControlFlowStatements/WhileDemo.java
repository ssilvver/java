package com.kosa.ControlFlowStatements;
//https://docs.oracle.com/javase/tutorial/java/nutsandbolts/while.html
class WhileDemo {
    public static void main(String[] args){
        int count = 1;
        while (count < 11) {
            System.out.println("Count is: " + count);
            count++;
        }
    }
}