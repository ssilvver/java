# Know Your Variables

variables must have a type

variables must have a name

'''	

	~
	int x  = 1 ; // 해당 행을 통해 메모리 주소, 변수명(또는 별명), 데이터의 타입, 데이터 이 결정된다고 할수있다. 
	~

'''

## primitive Types of Data
boolean and char : boolean, char

numeric

-Integer : byte, short, int, long

-floating point : float, double

! 참고 ! float을 경우 f 필수 예시 float number=32.5f;

## Java Tutorial
Especially for primitive type

### Variables
https://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html

포인트1 : variable과 field

variable에는 local variables, parameter가 포함되지만, field는 그렇지 못하므로, 이런점에서 variable이 더 큰 범주다.

한편, member라는 용어도 있으나, 위와는 다른 관점의 접근

type's fields, methods, and nested types are collectively called its members

### features based on Datatype
https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html

만약 필드에서 선언된 primitive data types들의 경우, 개발자가 초기값을 주지 않더라도, 컴파일러가 합리적인 초기값을 설정해준다. 하지만 field가 아닌, variables인 local variables는 초기화해주지 않는다. 당장은 에러가 안뜨더라도, 초기화되지 않은 지역변수를 다른 함수에서 아규먼트로 받아들이면 에러가 뜨는것을 확인가능하고 이는 초기화를 하지 않아서 이다.

### Arrays
https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html

container object that holds a fixed number of values of a single type

## reference Types of Data
Not primitive Type of java data type, that means, reference of object