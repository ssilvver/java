package chap01.practice;

public class PoolPuzzleOne {
	public static void main(String[] args) {
		int x=0;
		
		while(x<4) {
			// 모든 문장의 첫번째 캐릭터가 a로 시작되었다!
			System.out.print("a");
			if(x<1) {
				System.out.print(" ");
			}
			// 빈줄이 2줄인 주어진 조건에 맞추어 코딩
			if(x>1) {
				System.out.print(" oyster");
				x=x+2;
			}
			
			if(x==1) {
				System.out.print("nnoys");
			}
			
			// 내가 짜는 코드라면 하단의 같은 조건의 if문은 모아버렸겠지만,
			// 중요한건은 '주어진 조건 상황'에 맞추어였다. 이에 따라 아래와같이 작성한다.
			if(x<1) {
				System.out.print("nolse");
			}
			
			System.out.println("");
			x = x + 1;
		}

	}
}
